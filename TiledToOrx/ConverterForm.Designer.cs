﻿namespace TiledToOrx
{
    partial class ConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConverterForm));
            this.label1 = new System.Windows.Forms.Label();
            this.tmxTextBox = new System.Windows.Forms.TextBox();
            this.tiledBrowseButton = new System.Windows.Forms.Button();
            this.clipboardButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.mapOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.previewBox = new System.Windows.Forms.TextBox();
            this.checkboxPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.mapEntitiesPerRowBox = new System.Windows.Forms.NumericUpDown();
            this.lastLoadedTextbox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.checkboxPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapEntitiesPerRowBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select Tiled TMX File";
            // 
            // tmxTextBox
            // 
            this.tmxTextBox.Enabled = false;
            this.tmxTextBox.Location = new System.Drawing.Point(159, 59);
            this.tmxTextBox.Name = "tmxTextBox";
            this.tmxTextBox.Size = new System.Drawing.Size(339, 20);
            this.tmxTextBox.TabIndex = 3;
            this.tmxTextBox.TextChanged += new System.EventHandler(this.tmxTextBox_TextChanged);
            // 
            // tiledBrowseButton
            // 
            this.tiledBrowseButton.Location = new System.Drawing.Point(504, 59);
            this.tiledBrowseButton.Name = "tiledBrowseButton";
            this.tiledBrowseButton.Size = new System.Drawing.Size(24, 20);
            this.tiledBrowseButton.TabIndex = 4;
            this.tiledBrowseButton.Text = "...";
            this.tiledBrowseButton.UseVisualStyleBackColor = true;
            this.tiledBrowseButton.Click += new System.EventHandler(this.tiledBrowseButton_Click);
            // 
            // clipboardButton
            // 
            this.clipboardButton.Enabled = false;
            this.clipboardButton.Location = new System.Drawing.Point(421, 154);
            this.clipboardButton.Name = "clipboardButton";
            this.clipboardButton.Size = new System.Drawing.Size(106, 23);
            this.clipboardButton.TabIndex = 10;
            this.clipboardButton.Text = "Copy to Clipboard";
            this.clipboardButton.UseVisualStyleBackColor = true;
            this.clipboardButton.Click += new System.EventHandler(this.clipboardButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(540, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.helpToolStripMenuItem1.Text = "How to use";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TiledToOrx.Properties.Resources.tiled;
            this.pictureBox1.Location = new System.Drawing.Point(12, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 65);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::TiledToOrx.Properties.Resources.orx;
            this.pictureBox2.Location = new System.Drawing.Point(53, 112);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(94, 50);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // mapOnlyCheckBox
            // 
            this.mapOnlyCheckBox.AutoSize = true;
            this.mapOnlyCheckBox.Location = new System.Drawing.Point(0, 0);
            this.mapOnlyCheckBox.Name = "mapOnlyCheckBox";
            this.mapOnlyCheckBox.Size = new System.Drawing.Size(71, 17);
            this.mapOnlyCheckBox.TabIndex = 14;
            this.mapOnlyCheckBox.Text = "Map Only";
            this.mapOnlyCheckBox.UseVisualStyleBackColor = true;
            this.mapOnlyCheckBox.CheckedChanged += new System.EventHandler(this.mapOnlyCheckBox_CheckedChanged);
            // 
            // previewBox
            // 
            this.previewBox.BackColor = System.Drawing.SystemColors.Window;
            this.previewBox.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.previewBox.HideSelection = false;
            this.previewBox.Location = new System.Drawing.Point(12, 183);
            this.previewBox.Multiline = true;
            this.previewBox.Name = "previewBox";
            this.previewBox.ReadOnly = true;
            this.previewBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.previewBox.Size = new System.Drawing.Size(515, 250);
            this.previewBox.TabIndex = 17;
            this.previewBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.previewBox_KeyUp);
            // 
            // checkboxPanel
            // 
            this.checkboxPanel.Controls.Add(this.label2);
            this.checkboxPanel.Controls.Add(this.mapEntitiesPerRowBox);
            this.checkboxPanel.Controls.Add(this.mapOnlyCheckBox);
            this.checkboxPanel.Enabled = false;
            this.checkboxPanel.Location = new System.Drawing.Point(159, 85);
            this.checkboxPanel.Name = "checkboxPanel";
            this.checkboxPanel.Size = new System.Drawing.Size(189, 49);
            this.checkboxPanel.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Map entities per row";
            // 
            // mapEntitiesPerRowBox
            // 
            this.mapEntitiesPerRowBox.Location = new System.Drawing.Point(109, 23);
            this.mapEntitiesPerRowBox.Name = "mapEntitiesPerRowBox";
            this.mapEntitiesPerRowBox.Size = new System.Drawing.Size(80, 20);
            this.mapEntitiesPerRowBox.TabIndex = 20;
            this.mapEntitiesPerRowBox.ValueChanged += new System.EventHandler(this.mapEntitiesPerRow_ValueChanged);
            // 
            // lastLoadedTextbox
            // 
            this.lastLoadedTextbox.BackColor = System.Drawing.SystemColors.Control;
            this.lastLoadedTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lastLoadedTextbox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lastLoadedTextbox.Enabled = false;
            this.lastLoadedTextbox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lastLoadedTextbox.Location = new System.Drawing.Point(211, 159);
            this.lastLoadedTextbox.Name = "lastLoadedTextbox";
            this.lastLoadedTextbox.ReadOnly = true;
            this.lastLoadedTextbox.Size = new System.Drawing.Size(192, 13);
            this.lastLoadedTextbox.TabIndex = 19;
            this.lastLoadedTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 445);
            this.Controls.Add(this.lastLoadedTextbox);
            this.Controls.Add(this.checkboxPanel);
            this.Controls.Add(this.previewBox);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.clipboardButton);
            this.Controls.Add(this.tiledBrowseButton);
            this.Controls.Add(this.tmxTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "ConverterForm";
            this.Text = "Tiled TMX to Orx Config v";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.checkboxPanel.ResumeLayout(false);
            this.checkboxPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapEntitiesPerRowBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tmxTextBox;
        private System.Windows.Forms.Button tiledBrowseButton;
        private System.Windows.Forms.Button clipboardButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox mapOnlyCheckBox;
        private System.Windows.Forms.Panel checkboxPanel;
        private System.Windows.Forms.TextBox lastLoadedTextbox;
        private System.Windows.Forms.TextBox previewBox;
        private System.Windows.Forms.NumericUpDown mapEntitiesPerRowBox;
        private System.Windows.Forms.Label label2;
    }
}

