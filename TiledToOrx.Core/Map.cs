﻿using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace TiledToOrx.Core
{
    [Serializable, XmlRoot("map")]
    public class Map
    {
        [XmlAttribute(AttributeName = "width")]
        public int width { get; set; }
        [XmlAttribute(AttributeName = "height")]
        public int height;
        [XmlAttribute(AttributeName = "tilewidth")]
        public int tileWidth;
        [XmlAttribute(AttributeName = "tileheight")]
        public int tileHeight;

        [XmlElement(ElementName = "tileset")]
        public List<TileSet> tileSets;
        [XmlElement(ElementName = "layer")]
        public List<Layer> layers;

    }

    [XmlRoot("image")]
    public class Image
    {
        [XmlAttribute(AttributeName = "source")]
        public string source;
        [XmlAttribute(AttributeName = "width")]
        public int width;
        [XmlAttribute(AttributeName = "height")]
        public int height;
    }

    [XmlRoot("tileset")]
    public class TileSet
    {

        [XmlAttribute(AttributeName = "name")]
        public string name;
        [XmlAttribute(AttributeName = "firstgid")]
        public int firstgid;
        [XmlAttribute(AttributeName = "tilewidth")]
        public int tileWidth;
        [XmlAttribute(AttributeName = "tileheight")]
        public int tileHeight;
        [XmlAttribute(AttributeName = "tilecount")]
        public int tileCount;
        [XmlAttribute(AttributeName = "columns")]
        public int columns;
        [XmlElement(ElementName = "image")]
        public Image image;
        [XmlAttribute(AttributeName = "source")]
        public string source;
        [XmlElement(ElementName = "tile")]
        public List<TileSetTile> tiles;

        [XmlRoot("tile")]
        public class TileSetTile
        {
            [XmlAttribute(AttributeName = "id")]
            public int id;
            [XmlElement(ElementName = "image")]
            public Image image;
            [XmlAttribute(AttributeName = "type")]
            public string objectType;
        }
    }

    [XmlRoot("layer")]
    public class Layer
    {
        [XmlAttribute(AttributeName = "name")]
        public string name;
        [XmlAttribute(AttributeName = "width")]
        public int width;
        [XmlAttribute(AttributeName = "height")]
        public int height;
        [XmlElement(ElementName = "data")]
        public Data data;

        [XmlRoot("tile")]
        public class LayerTile
        {
            [XmlAttribute(AttributeName = "gid")]
            public int gid;
        }
    }

    [XmlRoot("data")]
    public class Data
    {
        private List<Int64> _mapData = new List<Int64>();
        private List<Layer.LayerTile> _tileData = new List<Layer.LayerTile>(); //when map is xml format
        private string _textData = "";

        [XmlAttribute(AttributeName = "encoding")]
        public string encoding;

        [XmlAttribute(AttributeName = "compression")]
        public string compression;

        public List<Int64> mapData {
            get {
                return _mapData;
            }
        }

        /// <summary>
        /// This is for when the tile map is supplied in XML format
        /// </summary>
        [XmlElement(ElementName = "tile")]
        public List<Layer.LayerTile> tiles;

        /// <summary>
        /// This is for when the tile map is supplied in csv, base64, gzip or zlib formats
        /// </summary>
        [XmlText]
        public string textData
        {
            get
            {
                return _textData;
            }
            set
            {
                string incomingData = value.Trim();

                _textData = incomingData;

                _mapData = new List<Int64>();

                if (this.encoding == "base64")
                {
                    byte[] bytes = Convert.FromBase64String(incomingData);

                    if (this.compression == "gzip")
                    {
                        MemoryStream stream = new MemoryStream(bytes);
                        System.IO.Compression.GZipStream gzipStream = new System.IO.Compression.GZipStream(stream, System.IO.Compression.CompressionMode.Decompress);
                        MemoryStream decompressedStream = new MemoryStream();

                        gzipStream.CopyTo(decompressedStream);

                        bytes = decompressedStream.ToArray();
                    }

                    if (this.compression == "zlib")
                    {
                        MemoryStream stream = new MemoryStream(bytes);

                        ZlibStream zlibStream = new ZlibStream(stream, Ionic.Zlib.CompressionMode.Decompress);
                        MemoryStream decompressedStream = new MemoryStream();

                        zlibStream.CopyTo(decompressedStream);

                        bytes = decompressedStream.ToArray();
                    }

                    for (int b = 0; b < bytes.Length; b+=4)
                    {
                        Int64 intValue = (bytes[b + 3] << 24)
                                    | (bytes[b + 2] << 16)
                                    | (bytes[b + 1] << 8)
                                    | bytes[b];

                        _mapData.Add(intValue);
                    }
                }

                if (this.encoding == "csv")
                {
                    string stringInts = incomingData;

                    if (stringInts == null)
                    {
                        return;
                    }

                    stringInts = stringInts.Replace("\n", "");

                    List<string> stringIntsList = stringInts.Split(',').ToList();

                    foreach (string stringInt in stringIntsList)
                    {
                        _mapData.Add(Convert.ToInt64(stringInt));
                    }
                }

            }
        }
    }

}
