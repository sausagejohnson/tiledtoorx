﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TiledToOrx.Core
{
    public class Engine
    {
        const string TAB = "\t";
        const string CR = "\n";
        const string AT = "@";

        private int entitiesPerRow = 0;
        private bool mapWidthEventAlreadyFired = false;
        public event PropertyChangedEventHandler MapEntityWidthPropertyChanged; //used by entity width box

        public FileSystemWatcher Watcher = new FileSystemWatcher();
        public StringBuilder Errors = new StringBuilder();

        public void Convert(string filePath, StringBuilder sb, bool mapOnly)
        {
            FileStream file = File.Open(filePath, FileMode.Open);

            XmlReaderSettings settings = new XmlReaderSettings()
            {
                DtdProcessing = DtdProcessing.Ignore
            };

            XmlReader reader = XmlReader.Create(file, settings);

            Watcher.Path = filePath.Replace(SourceToFileName(filePath), "");
            Watcher.Filter = SourceToFileName(filePath);
            Watcher.EnableRaisingEvents = true;



            Map map = SerialiseToMap(reader);
            ISet<string> objectTypes = new HashSet<string>();

            List<TileSet> externalTileSets = new List<TileSet>();
            foreach (TileSet tileset in map.tileSets)
            {
                if (tileset.source != null && tileset.source.Length > 0)
                {
                    TileSet tileSetPortion = ConvertExternalTileSetFile(filePath, tileset.source);
                    if (tileSetPortion.tiles == null)
                    {
                        break;
                    }

                    tileSetPortion.firstgid = tileset.firstgid;
                    tileSetPortion.source = tileset.source; ///FIX THIS

                    //For a single tile in a tileset, the column value is 0 causing div by 0 errors down the line.
                    //If there is at least one tile, make the column count = 1
                    if (tileSetPortion.columns == 0 && tileSetPortion.tileCount > 0)
                    {
                        tileSetPortion.columns = 1;
                    }

                    externalTileSets.Add(tileSetPortion);

                    // Extract the tiles' object type into a set
                    foreach (TileSet.TileSetTile tile in tileSetPortion.tiles)
                    {
                        if (tile.objectType != null)
                        {
                            objectTypes.Add(tile.objectType);
                        }
                    }
                }
            }

            if (externalTileSets.Count > 0)
            {
                map.tileSets.Clear();
                map.tileSets = externalTileSets;
            }

            file.Close();

            if (Errors.Length == 0)
            {
                CreateGraphicEntries(sb, map, objectTypes, mapOnly);
            }
        }

        //ImageCollection maps not currently supported. TODO: Remove this when supported.
        private bool TileSetContainsImageCollectionData(TileSet tileSet)
        {
            bool atLeastOneTileHasImageProperty = tileSet.tiles.Any(t => t.image != null);
            if (atLeastOneTileHasImageProperty == true)
            {
                Errors.AppendFormat("This map is based on a Collection of Images rather than a Tileset Image. TiledToOrx does not support this yet. Consider switching to a Tileset image for your maps. Call out on the Orx Discord if you require support for this format.");
            }

            return atLeastOneTileHasImageProperty;
        }

        private TileSet ConvertExternalTileSetFile(string filePath, string source)
        {
            FileStream tileSetFile = File.Open(Path.GetDirectoryName(filePath) + "/" + source, FileMode.Open);

            XmlReaderSettings settings = new XmlReaderSettings()
            {
                DtdProcessing = DtdProcessing.Ignore
            };

            XmlReader reader = XmlReader.Create(tileSetFile, settings);

            XmlSerializer serialiser = new XmlSerializer(typeof(TileSet));
            TileSet tileMapPortionMap = (TileSet)serialiser.Deserialize(reader);
            tileSetFile.Close();

            if (TileSetContainsImageCollectionData(tileMapPortionMap))
            {
                tileMapPortionMap = new TileSet(); //clear tileSet so it can't be used
            }

            return tileMapPortionMap;
        }

        public Map SerialiseToMap(XmlReader reader)
        {
            XmlSerializer serialiser = new XmlSerializer(typeof(Map));
            Map map = (Map)serialiser.Deserialize(reader);

            //If any xml tile data, convert it. This is post process due to a bug in the deserializer library
            foreach (Layer layer in map.layers)
            {
                if (layer.data.tiles.Count > 0) //there are xml tiles
                {
                    for (int x = 0; x < layer.data.tiles.Count; x++)
                    {
                        layer.data.mapData.Add(layer.data.tiles[x].gid);
                    }
                }
            }

            foreach (TileSet tileSet in map.tileSets)
            {

            }

            return map;
        }

        public string GetTileSetNameByIndex(Map map, Int64 index)
        {
            foreach (TileSet tileSet in map.tileSets)
            {
                if (index >= tileSet.firstgid && index <= (tileSet.firstgid + tileSet.tileCount - 1))
                {
                    return tileSet.name;
                }
            }
            return "NONE"; //return blank if no tile defined. ERROR RETURNS ?
        }

        public void CreateGraphicEntries(StringBuilder sb, Map map, ISet<string> objectTypes, bool mapOnly)
        {
            HashSet<Tile> tilesInUse = new HashSet<Tile>();

            //Populate tilesInUse
            foreach (TileSet tileSet in map.tileSets)
            {
                if (TileSetContainsImageCollectionData(tileSet))
                {
                    return;
                }

                for (int index = 0; index < tileSet.tileCount; index++)
                {
                    Tile tile = new Tile();
                    tile.index = index + tileSet.firstgid;
                    tile.X = (index % tileSet.columns) * tileSet.tileWidth;
                    tile.Y = (index / tileSet.columns) * tileSet.tileHeight; //row * tileSet.tileHeight;
                    tile.TileSetName = tileSet.name;

                    //got a tile but now lets see if it's used in one of the layers

                    for (int i = 0; i < map.layers.Count; i++)
                    {
                        Layer layer = map.layers[i];

                        if (layer.data.mapData.Contains(tile.index))
                        {
                            // This is probably not efficient, but I don't know how to parse the XML list into a HashMap, where the id attribute becomes the key
                            TileSet.TileSetTile setTile = tileSet.tiles.Find(t => t.id + tileSet.firstgid + 1 == tile.index);
                            tile.objectType = setTile != null ? setTile.objectType : "DefaultTile";

                            tilesInUse.Add(tile);
                        }
                    }

                }
            }


            //Print the graphic section and texture properties
            if (!mapOnly)
            {
                foreach (TileSet tileSet in map.tileSets)
                {
                    sb.AppendLine(FormatConfigHeading(tileSet.name + "Graphic"));
                    sb.AppendLine(FormatConfigProperty("Texture", SourceToFileName(tileSet.image.source)));
                    sb.AppendLine(FormatConfigProperty("Pivot", "top left"));

                    string textureSizeValue = String.Format("({0}, {1}, 0)", map.tileWidth, map.tileHeight);
                    sb.AppendLine(FormatConfigProperty("TextureSize", textureSizeValue));

                    sb.AppendLine("");

                }

                //Output cut up graphic sections that the map list will use
                foreach (Tile tile in tilesInUse)
                {
                    if (tile == null)
                    {
                        Errors.AppendFormat("Having difficulty creating a Orx Graphic Config Section at tileset position x:{0} y:{1}", tile.X, tile.Y);
                        return;
                    }

                    sb.AppendLine(FormatConfigHeading(tile.TileSetName + (tile.index).ToString() + "Graphic" + AT + tile.TileSetName + "Graphic"));

                    string textureCornerValue = String.Format("({0}, {1}, 0)", tile.X, tile.Y);
                    sb.AppendLine(FormatConfigProperty("TextureOrigin", textureCornerValue));

                    sb.AppendLine("");
                }

                //create a default object
                sb.AppendLine(FormatConfigHeading("DefaultTile"));
                sb.AppendLine("");

                //all the objects from the tilesets
                foreach (string type in objectTypes)
                {
                    sb.AppendLine(FormatConfigHeading(type));
                    sb.AppendLine("");
                }

                //Output object sections
                foreach (Tile tile in tilesInUse)
                {
                    if (tile == null)
                    {
                        Errors.AppendFormat("Having difficulty creating a Orx Object Config Section at tileset position x:{0} y:{1}", tile.X, tile.Y);
                        return;
                    }

                    sb.AppendLine(FormatConfigHeading(tile.TileSetName + (tile.index).ToString() + AT + (tile.objectType != null ? tile.objectType : "DefaulTile")));

                    string graphic = tile.TileSetName + (tile.index).ToString() + "Graphic";
                    sb.AppendLine(FormatConfigProperty("Graphic", graphic));

                    sb.AppendLine("");
                }
            }

            //Create map outputs
            CreateMap(sb, map, tilesInUse);
        }

        private void CreateMap(StringBuilder sb, Map map, HashSet<Tile> tilesInUse)
        {
            if (EntitiesPerRow == 0)
            {
                EntitiesPerRow = map.width; //this will trigger on the front end
            }

            foreach (Layer layer in map.layers)
            {
                int mapSpreadIndex = 1;

                sb.AppendLine(FormatConfigHeading(layer.name));

                string mapList = "";

                int lineLimitIndex = 0;

                for (int index = 0; index < layer.data.mapData.Count; index++)
                {
                    if (index > 0 && lineLimitIndex < EntitiesPerRow)
                        mapList += "#";

                    if (lineLimitIndex == EntitiesPerRow)
                    {
                        sb.AppendLine(FormatConfigProperty("MapPart" + mapSpreadIndex.ToString(), mapList));
                        mapSpreadIndex++;
                        lineLimitIndex = 0;
                        mapList = "";
                    }

                    lineLimitIndex++;
                    if (layer.data.mapData[index] == 0) //0 in the mapdata means no tile placed... essentially null
                    {
                        mapList += "EMPTY ";
                    }
                    else
                    {
                        mapList += GetTileSetNameByIndex(map, layer.data.mapData[index]) + (layer.data.mapData[index]).ToString() + " ";
                    }
                }

                sb.AppendLine(FormatConfigProperty("MapPart" + mapSpreadIndex.ToString(), mapList));
                sb.AppendLine("");
            }

        }

        private string FormatConfigHeading(string headingText)
        {
            return String.Format("[{0}]", headingText);
        }

        private string FormatConfigProperty(string parameter, string value)
        {
            return String.Format("{0}{1}= {2}", parameter, TAB, value);
        }

        private string SourceToFileName(string pathAndFile)
        {
            string[] pathFragments = pathAndFile.Split('/');
            if (pathFragments.Length == 1)
            {
                pathFragments = pathAndFile.Split('\\');
            }
            string fileName = pathFragments[pathFragments.Length - 1];

            return fileName;
        }

        public int EntitiesPerRow {
            get
            {
                return entitiesPerRow;
            }
            set {
                entitiesPerRow = value;
                if (mapWidthEventAlreadyFired == false)
                {
                    mapWidthEventAlreadyFired = true;
                    if (MapEntityWidthPropertyChanged != null)
                    {
                        MapEntityWidthPropertyChanged(this, new PropertyChangedEventArgs("SomethingTest"));
                    }
                }
            }
        }

    }
}
